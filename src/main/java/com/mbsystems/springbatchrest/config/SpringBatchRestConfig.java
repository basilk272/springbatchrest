package com.mbsystems.springbatchrest.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBatchProcessing
public class SpringBatchRestConfig {

    private final JobBuilderFactory jobBuilderFactory;

    private final StepBuilderFactory stepBuilderFactory;

    @Autowired
    public SpringBatchRestConfig( JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory ) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
    }

    @Bean
    public Step step1() {
        return this.stepBuilderFactory.get( "Step1" )
                        .tasklet( ( ( stepContribution, chunkContext ) -> {
                            System.out.println( "Step1 ran today" );

                            return RepeatStatus.FINISHED;
                        }) ).build();
    }

    @Bean
    public Job job() {
        return this.jobBuilderFactory.get( "Job" )
                        .incrementer( new RunIdIncrementer() )
                        .start( step1() )
                        .build();
    }
}
