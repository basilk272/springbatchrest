package com.mbsystems.springbatchrest.domain;

import lombok.Builder;
import lombok.Data;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;

import java.util.Properties;


public class JobLaunchRequest {

    private String name;

    private Properties jobParameters;

    public JobLaunchRequest( String name, Properties jobParameters ) {
        this.name = name;
        this.jobParameters = jobParameters;
    }

    public String getName() {
        return name;
    }

    public JobParameters getJobParameters() {
        Properties properties = new Properties();

        properties.putAll( this.jobParameters );

        return new JobParametersBuilder( properties ).toJobParameters();
    }
}
