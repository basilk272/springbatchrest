package com.mbsystems.springbatchrest.controller;

import com.mbsystems.springbatchrest.domain.JobLaunchRequest;
import com.mbsystems.springbatchrest.service.JobLaunchingService;
import org.springframework.batch.core.ExitStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JobLaunchingController {

    private final JobLaunchingService jobLaunchingService;

    public JobLaunchingController( JobLaunchingService jobLaunchingService ) {
        this.jobLaunchingService = jobLaunchingService;
    }

    @PostMapping( path = "/run" )
    public ExitStatus runJob( @RequestBody JobLaunchRequest jobLaunchRequest ) throws Exception {
        return this.jobLaunchingService.runJob( jobLaunchRequest );
    }
}
