package com.mbsystems.springbatchrest.service;

import com.mbsystems.springbatchrest.domain.JobLaunchRequest;
import org.springframework.batch.core.ExitStatus;

public interface JobLaunchingService {

    ExitStatus runJob( JobLaunchRequest JobLaunchRequest ) throws Exception;
}
