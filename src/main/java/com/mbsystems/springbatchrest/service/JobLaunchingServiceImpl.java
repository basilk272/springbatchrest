package com.mbsystems.springbatchrest.service;

import com.mbsystems.springbatchrest.domain.JobLaunchRequest;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class JobLaunchingServiceImpl implements JobLaunchingService {

    private final JobLauncher jobLauncher;

    private final ApplicationContext context;

    private final JobExplorer jobExplorer;

    @Autowired
    public JobLaunchingServiceImpl( JobLauncher jobLauncher, ApplicationContext context, JobExplorer jobExplorer ) {
        this.jobLauncher = jobLauncher;
        this.context = context;
        this.jobExplorer = jobExplorer;
    }

    @Override
    public ExitStatus runJob( JobLaunchRequest jobLaunchRequest ) throws Exception {
        Job job = this.context.getBean( jobLaunchRequest.getName(), Job.class );

        JobParameters jobParameters = new JobParametersBuilder( jobLaunchRequest.getJobParameters(), this.jobExplorer )
                                                .getNextJobParameters( job )
                                                .toJobParameters();

        return this.jobLauncher.run( job, jobParameters )
                        .getExitStatus();
    }
}
